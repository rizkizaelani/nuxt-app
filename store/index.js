import Vuex from  'vuex';

const store = ()  =>  {
return new Vuex.Store({
    state: {
        users: []
    },
    getters: {
        firstName(state) {
          return state.firstName;
        }
    },
    mutations: {
        users: state => state.users
    }
})
}

export default store